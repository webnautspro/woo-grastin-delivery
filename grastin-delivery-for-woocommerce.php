<?php
/*
Plugin Name: Grastin delivery for WooCommerce
Description:
Version: 1.0
Author: NGMTi
Author URI: mailto:proginc@inbox.ru
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
*/

if ( ! defined( 'ABSPATH' ) ) exit;

if (in_array('woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' )))) {

	function gdfw_shipping_method_init() {
		if ( ! class_exists( 'WC_gdfw_Post_Calc_Method' ) ) {
			class WC_gdfw_Post_Calc_Method extends WC_Shipping_Method {

				public function __construct( $instance_id = 0 ) {
					$this->cache              = false;
					$this->log                = true;
					$this->id                 = 'gdfw_post_calc';
					$this->instance_id        = absint( $instance_id );
					$this->method_title       = __( 'Grastin Delivery', 'gdfw-post-calc' );
					$this->method_description = __( 'The plugin for calculating delivery via Grastin service.', 'gdfw-post-calc' );
					$this->supports           = array(
					'shipping-zones',
					'instance-settings'
					);

			    	$this->instance_form_fields  = array(
		        		'title' => array(
		        			'title' 		=> __( 'Title', 'gdfw-post-calc' ),
		        			'type' 			=> 'text',
		        			'default'		=> __( 'Grastin Delivery', 'gdfw-post-calc' ),
		        		),
		        		'fixedvalue_disable' => array(
		        			'title' 		=> __( 'Min. cost of order', 'gdfw-post-calc' ),
		        			'description' 	=> __( 'Disable this method if the cost of the order is less than inputted value. Leave this field empty to allow any order cost.', 'gdfw-post-calc'),
		        			'type' 			=> 'number',
		        		)
					);

					$this->api                = 'c7b713ff-c160-4b13-a2e9-c794ef3d6ae6';
					$this->fixedvalue_disable = $this->get_option( 'fixedvalue_disable' );
					$this->kindList   		  = array(
						'grastincourier' => array('name' => 'Grastin', 'type' => 'kur', 'list' => false),
						'grastinpikup' => array('name' => 'Grastin', 'type' => 'pvz', 'list' => true),
						'post' => array('name' => 'Почта РФ', 'type' => 'pvz', 'list' => false),
						'postpackageonline' => array('name' => 'Почта РФ (online)', 'type' => 'pvz', 'list' => false),
						'postcourieronline' => array('name' => 'Почта РФ (Курьер)', 'type' => 'kur', 'list' => false),
						'boxberrypikup' => array('name' => 'Boxberry', 'type' => 'pvz', 'list' => true),
						'boxberrycourier' => array('name' => 'Boxberry', 'type' => 'kur', 'list' => false),
						'hermespikup' => array('name' => 'Hermes', 'type' => 'pvz', 'list' => true)
					);

					add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );

				}

				public function _isCurl(){
				    return function_exists('curl_version');
				}

				public function log( $text ) {
					if( $this->log ) {
						$debug = ":::: Date: " . date("d.m.Y H:i:s") . PHP_EOL;
			            $debug .= $text . PHP_EOL;

			            $f = fopen($_SERVER['DOCUMENT_ROOT'] . "/wp-content/plugins/grastin-delivery-for-woocommerce/log/log_grastin.log", "a");
			            fwrite($f, $debug);
			            fclose($f);
			        }
				}

				public function calculate_shipping( $package = array() ) {
					$from = 'Санкт-Петербург';

					$api = $this->api;
					if ( empty($api) )  {
						return;
					}

					global $woocommerce;

					$city = $package['destination']['city'];
					if ( !$city || $city == '' ) {
						return;
					}

					if(in_array($city, array('Питер', 'Петербург'))) $city = 'Санкт-Петербург';

					$site_url    = get_option('siteurl');
					$site_url    = implode('/', array_slice(explode('/', preg_replace('/https?:\/\/|www./', '', $site_url)), 0, 1));

					$weight      = $woocommerce->cart->cart_contents_weight;
					$weight      = wc_get_weight( $weight, 'g' );
					if( $weight == 0 ) {
						$weight = 1000;
					}

					$totalval = $package['contents_cost'];
					if ($this->fixedvalue_disable != '' && $this->fixedvalue_disable > 0) {
						if ($totalval < $this->fixedvalue_disable) {
							return;
						}
					}

					$Request = '<File>';
					$Request .= '<API>'.$api.'</API>';
					$Request .= '<Method>deliverycost</Method>';
					$Request .= '<weight>'.$weight.'</weight>';
					$Request .= '<cityfrom>'.$from.'</cityfrom>';
					$Request .= '<cityto>'.$city.'</cityto>';
					$Request .= '</File>';

					$urlApi = "http://api.grastin.ru/api.php";

					if( $this->cache ) {
						$cachename = md5($from.'-'.$city.'-'.$weight);
						$arrResponse = get_transient( $cachename );
					} else {
						$arrResponse = false;
					}

					$this->log( "Request: " . $Request );

					if ( false === $arrResponse ) {
						$Response = $this->connect_api_server($Request, $urlApi);
						if ( !$Response ) {
							return;
						}

						$arrResponse = $this->unxml($Response);
						if ( !count($arrResponse) ) {
							return;
						}

						if( $this->cache ) {
							set_transient( $cachename, $arrResponse, HOUR_IN_SECONDS );
						}
					}

					$rates = array();
					foreach ($arrResponse as $code => $value) {
						$uid = strtolower($value['uid']);

						if(array_key_exists($uid, $this->kindList)) {
							$rate_id = $this->get_rate_id() . ':' . $uid;

							$exRate = array(
								'id'    => $rate_id,
								'label' => $this->kindList[$uid]['name'] . '#' . $value['DeliveryPeriod'],
								'cost'  => $value['Cost']
							);

							$this->add_rate( $exRate );
						}
					}
				}

				public function connect_api_server($Request, $apiUrl) {

					if ($this->_isCurl()) {
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $apiUrl);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'XMLPackage='.urlencode($Request));
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
						$result = curl_exec($ch);
						curl_close($ch);

						$this->log( "result: " . $result );

						return $result;
					} else {
						return false;
					}
				}

				public function unxml($Response) {
					$results = array();

					$xmlDecoder = new SimpleXMLElement($Response);
					if($xmlDecoder) {
						if(isset($xmlDecoder->DeliveryCost)) {
							foreach ($xmlDecoder->DeliveryCost as $cost) {
								$results[] = array(
									'kind' => (string)$cost->Kind,
									'uid' => (string)$cost->uid,
									'Id' => (string)$cost->Id,
									'Cost' => (float)$cost->Cost,
									'DeliveryPeriod' => (int)$cost->DeliveryPeriod
								);
							}
						}
					}

					return $results;
				}
			}
		}
	}
	add_action( 'woocommerce_shipping_init', 'gdfw_shipping_method_init' );

	function gdfw_add_post_calc_shipping_method( $methods ) {
		$methods[ 'gdfw_post_calc' ] = 'WC_gdfw_Post_Calc_Method';
		return $methods;
	}
	add_filter( 'woocommerce_shipping_methods', 'gdfw_add_post_calc_shipping_method' );

	/* START OF CUSTOM INPUT */
	add_action( 'woocommerce_after_order_notes', 'gdfw_add_custom_field' );
	function gdfw_add_custom_field( $checkout ) {
	    woocommerce_form_field( 'gdfw_shipping_method', array(
			'type'     => 'text',
			'class'    => array('my-field-class form-row-wide'),
			'required' => true
	    ), $checkout->get_value( 'gdfw_shipping_method' ));

	    /*woocommerce_form_field( 'gdfw_shippingdate', array(
			'type'     => 'text',
			'class'    => array('my-field-class form-row-wide'),
			'required' => true
	    ), $checkout->get_value( 'gdfw_shippingdate' ));*/
	}

	add_action('woocommerce_checkout_process', 'gdfw_custom_field_process');
	function gdfw_custom_field_process() {
		if ( ! $_POST['gdfw_shipping_method'] )
		    wc_add_notice( 'Выберите способ или пункт доставки', 'error' );

		if ( ! $_POST['gdfw_shippingdate'] ) {
		    wc_add_notice( 'Укажите желаемую дату доставки', 'error' );
		} else {
			if( strtotime($_POST['gdfw_shippingdate']) < time() ) {
				wc_add_notice( 'Некорректная дата доставки', 'error' );
			}
		}
	}

	add_action( 'woocommerce_checkout_update_order_meta', 'gdfw_custom_field_update' );
	function gdfw_custom_field_update( $order_id ) {
		if ( ! empty( $_POST['gdfw_shipping_method'] ) ) {
		    update_post_meta( $order_id, 'gdfw_shipping_method', sanitize_text_field( $_POST['gdfw_shipping_method'] ) );
		}

		if ( ! empty( $_POST['gdfw_shippingdate'] ) ) {
		    update_post_meta( $order_id, 'gdfw_shippingdate', sanitize_text_field( $_POST['gdfw_shippingdate'] ) );
		}
	}

	add_action( 'woocommerce_admin_order_data_after_billing_address', 'gdfw_custom_field_view', 10, 1 );
	function gdfw_custom_field_view($order){
		$gdfw_k = get_post_meta( $order->id, 'gdfw_shipping_method', true );
		if($gdfw_k) {
			$gdfw_k_ex = explode("|", $gdfw_k);

		    echo '<p><strong>Доставка:</strong><br>';
		    echo $gdfw_k_ex[2] . ' - ' . $gdfw_k_ex[5] . ' руб. срок: ' . $gdfw_k_ex[6] . ' дня(ей)<br>';
		    if(isset($gdfw_k_ex[7]) && !empty($gdfw_k_ex[7])) {
		    	echo 'Место самовывоза: ' . $gdfw_k_ex[7];
		    }
		    echo '</p>';
	    }
	}
	/* END OF CUSTOM INPUT */

	/* FORM SEND ZK */
	function gdfw_send_box () {
	    add_meta_box( 'gdfw_send_box', 'Отправить заказ', 'gdfw_send_box_func', 'shop_order', 'side', 'high' );
	}
	add_action( 'add_meta_boxes', 'gdfw_send_box' );

	function gdfw_send_box_func() {
	    global $post;

	    $gdfw_shipping_method_str = get_post_meta($post->ID, 'gdfw_shipping_method', true);
	    $gdfw_shippingdate_str = get_post_meta($post->ID, 'gdfw_shippingdate', true);

	    if($gdfw_shipping_method_str) {
		    $gdfw_shipping_method = explode("|", $gdfw_shipping_method_str);

		    $wc_order = wc_get_order( $post->ID );
		    $order = $wc_order->get_data();

		    if(!empty($gdfw_shippingdate_str)) {
		    	$gdfw_shippingdate_str_formate = date("dmY", strtotime($gdfw_shippingdate_str));
		    } else {
		    	$gdfw_shippingdate_str_formate = "";
		    }

		    switch ($gdfw_shipping_method[3]) {
		    	case 'grastincourier':
		    	case 'grastinpikup':
		    		$html = '<p><label>Адрес доставки:</label><br><input type="text" name="gdfw_input[address]" value="' . $order['billing']['city'] . ' ' . $order['billing']['address_1'] . '"/></p>';
		    		$html .= '<p><label>Комментарий по доставке (не обязательно):</label><br><textarea name="gdfw_input[comment]"></textarea></p>';
		    		$html .= '<p><label>Дата доставки:</label><br><input type="text" name="gdfw_input[shippingdate]" value="'. $gdfw_shippingdate_str_formate .'" placeholder="Внимание! Формат ДДММГГГГ" /></p>';
		    		$html .= '<p><label>Имя покупателя:</label> <input type="text" name="gdfw_input[buyer]" value="' . $order['billing']['first_name'] . ' ' . $order['billing']['last_name'] . '"/></p>';
		    		$html .= '<p><label>Номер телефона:</label> <input type="text" name="gdfw_input[phone1]" value="' . $order['billing']['phone'] . '"/></p>';
		    		$html .= '<p><label>Количество мест:</label> <input type="text" name="gdfw_input[seats]" value="1"/></p>';
		    		$html .= '<p><label>Склад приёма заказа:</label> <input type="text" name="gdfw_input[takewarehouse]" value="Санкт-Петербург"/></p>';
		    		$html .= '<p><label>Вид груза:</label> <input type="text" name="gdfw_input[cargotype]" value="Сборные модели из картона"/></p>';
		    		$html .= '<p><label>Почта клиента:</label> <input type="text" name="gdfw_input[email]" value="' . $order['billing']['email'] . '"/></p>';
		    		$html .= '<input type="hidden" name="gdfw_input[number]" value="VYL' . $order['id'] . '" />
		    		<input type="hidden" name="gdfw_input[summa]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[assessedsumma]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[test]" value="no"/>';
		    		if($gdfw_shipping_method[3] == "grastincourier") {
		    			$html .= '<input type="hidden" name="gdfw_input[service]" value="3" />';
		    		} else {
		    			$html .= '<input type="hidden" name="gdfw_input[service]" value="7" />';
		    		}
		    		break;

		    	case 'post':
		    	case 'postpackageonline':
		    	case 'postcourieronline':
		    		$html = '<p><label>Комментарий по доставке (не обязательно):</label><br><textarea name="gdfw_input[comment]"></textarea></p>';
		    		$html .= '<p><label>Дата доставки:</label><br><input type="text" name="gdfw_input[shippingdate]" value="'. $gdfw_shippingdate_str_formate .'" placeholder="Внимание! Формат ДДММГГГГ" /></p>';
		    		$html .= '<p><label>Имя покупателя:</label> <input type="text" name="gdfw_input[buyer]" value="' . $order['billing']['first_name'] . ' ' . $order['billing']['last_name'] . '"/></p>';
		    		$html .= '<p><label>Номер телефона:</label> <input type="text" name="gdfw_input[phone]" value="' . $order['billing']['phone'] . '"/></p>';
		    		$html .= '<p><label>Индекс:</label> <input type="text" name="gdfw_input[zipcode]" value="' . $order['billing']['postcode'] . '"/></p>';
		    		$html .= '<p><label>Регион:</label> <input type="text" name="gdfw_input[region]" value=""/></p>';
		    		$html .= '<p><label>Район:</label> <input type="text" name="gdfw_input[district]" value=""/></p>';
		    		$html .= '<p><label>Город:</label> <input type="text" name="gdfw_input[city]" value="' . $order['billing']['city'] . '"/></p>';
		    		$html .= '<p><label>Адрес:</label> <input type="text" name="gdfw_input[address]" value="' . $order['billing']['address_1'] . '"/></p>';
		    		$html .= '<p><label>Склад приёма заказа:</label> <input type="text" name="gdfw_input[takewarehouse]" value="Санкт-Петербург"/></p>';
		    		$html .= '<p><label>Вид груза:</label> <input type="text" name="gdfw_input[cargotype]" value="Одежда (вязаные изделия, текстиль) и аксессуары (шапки, шарфы)"/></p>';
		    		$html .= '<p><label>Почта клиента:</label> <input type="text" name="gdfw_input[email]" value="' . $order['billing']['email'] . '"/></p>';
		    		$html .= '<input type="hidden" name="gdfw_input[number]" value="VYL' . $order['id'] . '" />
		    		<input type="hidden" name="gdfw_input[cod]" value="no"/>
		    		<input type="hidden" name="gdfw_input[summa]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[value]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[test]" value="no"/>';

		    		if($gdfw_shipping_method[3] == "post") {
		    			$html .= '<input type="hidden" name="gdfw_input[service]" value="13" />';
		    		} elseif($gdfw_shipping_method[3] == "postpackageonline") {
		    			$html .= '<input type="hidden" name="gdfw_input[service]" value="14" />';
		    		} else {
		    			$html .= '<input type="hidden" name="gdfw_input[service]" value="15" />';
		    		}
		    		break;
		    	case 'boxberrypikup':
		    	case 'boxberrycourier':
		    		$html = '<p><label>Комментарий по доставке (не обязательно):</label><br><textarea name="gdfw_input[comment]"></textarea></p>';
		    		$html .= '<p><label>Номер телефона:</label> <input type="text" name="gdfw_input[phone1]" value="' . $order['billing']['phone'] . '"/></p>';
		    		$html .= '<p><label>Имя покупателя:</label> <input type="text" name="gdfw_input[buyer]" value="' . $order['billing']['first_name'] . ' ' . $order['billing']['last_name'] . '"/></p>';
		    		if($gdfw_shipping_method[3] == "boxberrycourier") {
		    			$html .= '<p><label>Адрес доставки:</label> <input type="text" name="gdfw_input[address]" value="' . $order['billing']['address_1'] . '"/></p>';
		    		}
		    		$html .= '<p><label>Количество мест:</label> <input type="text" name="gdfw_input[seats]" value="1"/></p>';
		    		$html .= '<p><label>Вид груза:</label> <input type="text" name="gdfw_input[cargotype]" value="Одежда (вязаные изделия, текстиль) и аксессуары (шапки, шарфы)"/></p>';
		    		$html .= '<p><label>Склад приёма заказа:</label> <input type="text" name="gdfw_input[takewarehouse]" value="Санкт-Петербург"/></p>';
		    		$html .= '<p><label>Почта клиента:</label> <input type="text" name="gdfw_input[email]" value="' . $order['billing']['email'] . '"/></p>';

		    		$html .= '<input type="hidden" name="gdfw_input[number]" value="VYL' . $order['id'] . '" />
		    		<input type="hidden" name="gdfw_input[weight]" value="1000"/>
		    		<input type="hidden" name="gdfw_input[postcode]" value=""/>';
		    		if($gdfw_shipping_method[0]) {
		    			$html .= '<input type="hidden" name="gdfw_input[pickup]" value="'.$gdfw_shipping_method[0].'"/>';
		    		}
		    		if($gdfw_shipping_method[5]) {
		    			$html .= '<input type="hidden" name="gdfw_input[costdelivery]" value="'.$gdfw_shipping_method[5].'"/>';
		    		}
		    		$html .= '<input type="hidden" name="gdfw_input[summa]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[assessedsumma]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[test]" value="no"/>';
		    		break;
		    	case 'hermespikup':
		    		$html = '<p><label>Комментарий по доставке (не обязательно):</label><br><textarea name="gdfw_input[comment]"></textarea></p>';
		    		$html .= '<p><label>Номер телефона:</label> <input type="text" name="gdfw_input[phone1]" value="' . $order['billing']['phone'] . '"/></p>';
		    		$html .= '<p><label>Имя покупателя:</label> <input type="text" name="gdfw_input[buyer]" value="' . $order['billing']['first_name'] . ' ' . $order['billing']['last_name'] . '"/></p>';
		    		$html .= '<p><label>Количество мест:</label> <input type="text" name="gdfw_input[seats]" value="1"/></p>';
		    		$html .= '<p><label>Вид груза:</label> <input type="text" name="gdfw_input[cargotype]" value="Одежда (вязаные изделия, текстиль) и аксессуары (шапки, шарфы)"/></p>';
		    		$html .= '<p><label>Склад приёма заказа:</label> <input type="text" name="gdfw_input[takewarehouse]" value="Санкт-Петербург"/></p>';
		    		$html .= '<p><label>Почта клиента:</label> <input type="text" name="gdfw_input[email]" value="' . $order['billing']['email'] . '"/></p>';

		    		$html .= '<input type="hidden" name="gdfw_input[number]" value="VYL' . $order['id'] . '" />
		    		<input type="hidden" name="gdfw_input[summa]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[assessedsumma]" value="' . $order['total'] . '"/>
		    		<input type="hidden" name="gdfw_input[weight]" value="1000"/>';
		    		if($gdfw_shipping_method[0]) {
		    			$html .= '<input type="hidden" name="gdfw_input[pickup]" value="'.$gdfw_shipping_method[0].'"/>';
		    		}
		    		$html .= '<input type="hidden" name="gdfw_input[test]" value="no"/>';
		    		break;
		    }
		    $html .= '<input type="hidden" name="gdfw_input[param]" value="'.$gdfw_shipping_method_str.'"/>
		    <input type="hidden" name="gdfw_input[method]" value="'.$gdfw_shipping_method[3].'"/>';

		    echo $html;
	        echo '<p><input type="submit" class="add_note button" name="sendgrastin" value="Отправить заказ"></p>
	        <style>#gdfw_send_box input, #gdfw_send_box textarea { width: 100%; }</style>';
	    }
	}
	add_action('woocommerce_process_shop_order_meta', 'gdfw_save_box', 0, 2);

	function gdfw_save_box($post_id) {
		$api = 'ac483c0a-2ba5-405f-a78c-1d4a9cddf06f';

		if ( isset($_POST['sendgrastin']) && isset($_POST['gdfw_input']) ) {
			$data = $var = array();
			foreach ($_POST['gdfw_input'] as $key => $value) {
				if(in_array($key, array('param', 'method'))) {
					$var[$key] = sanitize_text_field($value);
				} else {
					$data[$key] = $key . '="' . sanitize_text_field($value) . '"';
				}
			}

			if($data && $var['method']) {
				$Response = false;

			    $wc_order = wc_get_order( $post_id );
			    $items = array();
			    $get_items = $wc_order->get_items();
			    foreach ($get_items as $item) {
			    	$items[] = $item->get_data();
			    }

	    		$Request = '<File>';
				$Request .= '<API>'.$api.'</API>';
			    switch ($var['method']) {
			    	case 'grastincourier':
			    	case 'grastinpikup':
			    		$total = 0;
						$Request .= '<Method>newordercourier</Method>';
						$Request .= '<Orders>';
						$Request .= '<Order ' . implode(" ", $data) . '>';
						foreach ($items as $item) {
							$Request .= '<good article="'. $item['product_id'] .'" name="'. htmlspecialchars($item['name']) .'" cost="'. $item['total'] .'" amount="'. $item['quantity'] .'" />';
							$total += $item['total'];
						}

						$costdelivery = $_POST['gdfw_input']['summa'] - $total;
						if( $costdelivery < 0 ) {
							$costdelivery = 0;
						}
			    		$Request .= '<good article="9999" name="Доставка" cost="'. $costdelivery .'" amount="1" />';

						$Request .= '</Order>';
						$Request .= '</Orders>';
			    		break;
			    	case 'post':
			    	case 'postpackageonline':
			    	case 'postcourieronline':
						$Request .= '<Method>newordermail</Method>';
						$Request .= '<Orders>';
						$Request .= '<Order ' . implode(" ", $data) . '>';
						foreach ($items as $item) {
							$Request .= '<good article="'. $item['product_id'] .'" name="'. htmlspecialchars($item['name']) .'" cost="'. $item['total'] .'" amount="'. $item['quantity'] .'" />';
						}
						$Request .= '</Order>';
						$Request .= '</Orders>';
				    	break;
			    	case 'boxberrypikup':
			    	case 'boxberrycourier':
						$Request .= '<Method>neworderboxberry</Method>';
						$Request .= '<Orders>';
						$Request .= '<Order ' . implode(" ", $data) . '>';
						foreach ($items as $item) {
							$Request .= '<good article="'. $item['product_id'] .'" name="'. htmlspecialchars($item['name']) .'" cost="'. $item['total'] .'" vat="0" amount="'. $item['quantity'] .'" />';
						}
						$Request .= '</Order>';
						$Request .= '</Orders>';
				    	break;
			    	case 'hermespikup':
						$Request .= '<Method>neworderhermes</Method>';
						$Request .= '<Orders>';
						$Request .= '<Order ' . implode(" ", $data) . '>';
						foreach ($items as $item) {
							$Request .= '<good article="'. $item['product_id'] .'" name="'. htmlspecialchars($item['name']) .'" cost="'. $item['total'] .'" amount="'. $item['quantity'] .'" />';
						}
						$Request .= '</Order>';
						$Request .= '</Orders>';
				    	break;
			    }
			    $Request .= '</File>';

				if(function_exists('curl_version')) {
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://api.grastin.ru/api.php");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'XMLPackage='.urlencode($Request));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$Response = curl_exec($ch);
					curl_close($ch);
				}

				$xmlDecoder = new SimpleXMLElement($Response);
				$comment_post_ID        = $post_id;
				$comment_author_url     = '';
				$comment_agent          = 'WooCommerce';
				$comment_type           = 'order_note';
				$comment_parent         = 0;
				$comment_approved       = 1;

				if($xmlDecoder && isset($xmlDecoder->Order->Status)) {
					$comment_content = 'Заказ был выгружен в Grastin - успешно, номер #' . $xmlDecoder->Order->number;
			    } elseif($xmlDecoder && isset($xmlDecoder->Order->Error)) {
			    	$comment_content = 'Заказ НЕ был выгружен в Grastin - ошибка: ' . $xmlDecoder->Order->Error;
			    }
			    $commentdata = apply_filters( 'woocommerce_new_order_note_data', compact( 'comment_post_ID', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comment_agent', 'comment_type', 'comment_parent', 'comment_approved' ), array( 'order_id' => $post_id, 'is_customer_note' => 0 ) );
			    wp_insert_comment( $commentdata );
			}
		}

	}
	/* END FORM */


	function gdfw_action_callback() {
		$shipping_method = (array)$_POST['methods'];

		if(is_array($shipping_method)) {
			$KindList = array(
				'grastincourier' => array('name' => 'Grastin', 'image' => plugins_url('assets/img/Grastin.png', __FILE__), 'type' => 'kur', 'list' => false),
				'grastinpikup' => array('name' => 'Grastin', 'image' => plugins_url('assets/img/Grastin.png', __FILE__), 'type' => 'pvz', 'list' => true),
				'post' => array('name' => 'Почта РФ', 'image' => plugins_url('assets/img/Post.png', __FILE__), 'type' => 'pvz', 'list' => false),
				'postpackageonline' => array('name' => 'Почта РФ (online)', 'image' => plugins_url('assets/img/Post.png', __FILE__), 'type' => 'pvz', 'list' => false),
				'postcourieronline' => array('name' => 'Почта РФ (Курьер)', 'image' => plugins_url('assets/img/Post.png', __FILE__), 'type' => 'kur', 'list' => false),
				'boxberrypikup' => array('name' => 'Boxberry', 'image' => plugins_url('assets/img/Boxberry.png', __FILE__), 'type' => 'pvz', 'list' => true),
				'boxberrycourier' => array('name' => 'Boxberry', 'image' => plugins_url('assets/img/Boxberry.png', __FILE__), 'type' => 'kur', 'list' => false),
				'hermespikup' => array('name' => 'Hermes', 'image' => plugins_url('assets/img/Hermes.png', __FILE__), 'type' => 'pvz', 'list' => true)
			);

			$methods = array();
			$checked = null;
			foreach ($shipping_method as $mt) {
				$mtExplode = explode(":", $mt['value']);
				$kind = trim(array_pop($mtExplode));

				if(array_key_exists($kind, $KindList)) {
					$mt['kind'] = $kind;

					$labelExplode = explode(":", $mt['label']);
					if($labelExplode > 1) {
						$cost = preg_replace("/[^0-9.]/", '', $labelExplode[1]);
						$cost = round($cost);

						$periodExplode = explode("#", $labelExplode[0]);
						$mt['label'] = $periodExplode[0];
						$mt['days'] = (int)$periodExplode[1];
						$mt['cost'] = $cost;

						$methods[] = array_merge($mt, $KindList[$kind]);
					}
				}
			}

			echo json_encode($methods);
		}
		wp_die();
	}
	add_action('wp_ajax_gdfw_action', 'gdfw_action_callback');
	add_action('wp_ajax_nopriv_gdfw_action', 'gdfw_action_callback');


	function gdfw_action_elements_callback() {
		$param = htmlspecialchars( (string)$_POST['param'] );
		$city = htmlspecialchars( (string)$_POST['city'] );
		$type = htmlspecialchars( (string)$_POST['type'] );

		$paramEx = explode("|", $param);

		if(!empty($city) && !empty($type) && is_array($paramEx)) {
			$cachename = md5($paramEx[1].'-'.$city);
			delete_transient( $cachename );
			$arrResponse = get_transient( $cachename );

			if ( false === $arrResponse ) {
				$Response = false;
				$api = 'ac483c0a-2ba5-405f-a78c-1d4a9cddf06f';

				if(in_array($city, array('Питер', 'Петербург'))) $city = 'Санкт-Петербург';

				$Request = '<File>';
				$Request .= '<API>'.$api.'</API>';
				switch ($paramEx[1]) {
					case 'grastinpikup':
						$keyxml = 'Selfpickup';

						$Request .= '<Method>selfpickup</Method>';
						if(in_array($city, array('Санкт-Петербург', 'Москва', 'Балашиха'))) {
							$Request .= '<City>'.$city.'</City>';
						}
						break;
					case 'boxberrypikup':
						$keyxml = 'SelfpickupBoxberry';

						$Request .= '<Method>boxberryselfpickup</Method>';
						$Request .= '<City>'.$city.'</City>';
						break;
					case 'hermespikup':
						$keyxml = 'SelfpickupHermes';

						$Request .= '<Method>hermesselfpickup</Method>';
						$Request .= '<City>'.$city.'</City>';
						break;
				}
				$Request .= '</File>';

				if(function_exists('curl_version')) {
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://api.grastin.ru/api.php");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, 'XMLPackage='.urlencode($Request));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					$Response = curl_exec($ch);
					curl_close($ch);
				}

				if ( $Response ) {
					$xmlDecoder = new SimpleXMLElement($Response);
					if($xmlDecoder && isset($xmlDecoder->{$keyxml})) {
				        foreach ($xmlDecoder->{$keyxml} as $cost) {
				        	if($paramEx[1] == "grastinpikup") {
				        		$nId = (string)$cost->id;
				        	} else {
				        		$nId = (string)$cost->Id;
				        	}

				        	$desc = '<div data-item="' . json_encode($cost) . '">' .str_replace(array("\r", "\n"), '', '<p>'.$cost->Name.'</p><p><strong>Время работы:</strong><br>'.$cost->schedule.'</p><p><strong>Как пройти:</strong><br>'.$cost->drivingdescription.'</p>') . '<p class="text-center"><a href="javascript:void(0)" class="gdfw-btn" data-id="'.$nId.'" data-type="'.$type.'" data-param="'.$paramEx[0].'|'.$paramEx[1].'|'.$paramEx[2].'|'.$paramEx[3].'|'.$paramEx[4].'" data-this="'.$cost->Name.'" onclick="gdfw_click(this);">Выбрать</a></p></div>';

				            $arrResponse[] = array(
				                'id' => $nId,
				                'title' => (string)$paramEx[0],
				                'name' => (string)$cost->Name,
				                'latitude' => (string)$cost->latitude,
				                'longitude' => (string)$cost->longitude,
				                'schedule' => (string)$cost->schedule,
				                'drivingdescription' => (string)$cost->drivingdescription,
				                'desc' => $desc
				            );
				        }
				        set_transient( $cachename, $arrResponse, DAY_IN_SECONDS );
					}
				}
			}

			echo json_encode($arrResponse);
		}
		wp_die();
	}
	add_action('wp_ajax_gdfw_action_elements', 'gdfw_action_elements_callback');
	add_action('wp_ajax_nopriv_gdfw_action_elements', 'gdfw_action_elements_callback');


	function gdfw_hook(){
		echo '<div id="grastinDelivery">
		<h3>Доставка</h3>
		<p>Выберите способ доставки. При выборе способа Самовывоз, на карте выберите удобное для вас место самовывоза, просто шелкнув на метку и нажав кнопку "Выбрать"</p>
		<div class="gdfw-block">
	        <div class="gdfw-block1">
	            <div class="gdfw-list-btn"></div>
	            <div id="gdfw-map"></div>
	            <div id="gdfw-list">
	                <ul class="gdfw-list-address"></ul>
	            </div>
	        </div>
	        <div class="gdfw-block2">
	            <div class="gdfw-tab">
	                <button type="button" class="gdfw-tab-kur active" data-type="kur" onclick="gdfw_change(this);">Курьер</button>
	                <button type="button" class="gdfw-tab-pvz" data-type="pvz" onclick="gdfw_change(this);">Самовывоз</button>
	            </div>
	            <ul class="gdfw-delivery"></ul>
	        </div>
	        <div class="gdfw-block-loading">Загрузка...</div>
	    </div>
	    <div class="form-row my-field-class form-row-wide validate-required" id="gdfw_shippingdate_field" data-priority="">
			<h4>Желаемая дата доставки</h4>
		    <input type="text" class="input-text" name="gdfw_shippingdate" id="gdfw_shippingdate" placeholder="" value="" /></div>
		</div>';
	}
	add_action( 'woocommerce_checkout_before_order_review', 'gdfw_hook' );


	function gdfw_js(){
		if(is_checkout()) {
			wp_enqueue_script( 'yandex-maps', '//api-maps.yandex.ru/2.1/?lang=ru_RU');
			wp_enqueue_style( 'gdfw-style', plugins_url('assets/css/style.css', __FILE__) );

		    $variables = array (
		        'ajax_url' => admin_url('admin-ajax.php'),
		    	'is_mobile' => wp_is_mobile()
		    );

		    echo (
		        '<script type="text/javascript">window.wp_data = ' .
		        json_encode($variables) .
		        ';</script>'
		    );
		}

	}
	add_action('wp_head','gdfw_js');

	function gdfw_script(){
		if(is_checkout()) {
			echo "<link href='".plugins_url('assets/css/datepicker.css', __FILE__)."' rel='stylesheet' type='text/css'>
			<script type='text/javascript' src='".plugins_url('assets/js/datepicker.js', __FILE__)."'></script>
			<script type='text/javascript' src='".plugins_url('assets/js/main.js', __FILE__)."'></script>
			";
		}

		if(is_cart()) {
			echo "<style> table.shop_table tr.shipping { display: none !important; } </style>";
		}
	}
	add_action( 'wp_footer', 'gdfw_script', 99);

}


add_action( 'plugins_loaded', 'gdfw_load_textdomain' );
function gdfw_load_textdomain() {
	load_plugin_textdomain( 'gdfw-post-calc', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
}