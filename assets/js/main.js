var myMap,
	myMapCenter,
	myPlacemarks,
	myGeoQuery;

function gdfw_onloader()
{
	gdfw_loader();

	jQuery("#gdfw_shipping_method").val("");
	jQuery("#gdfw_shipping_tr").remove();
	console.log('3');

	setTimeout(function() {
	var billing_city = jQuery("#billing_city").val();

	if(billing_city != "" && jQuery("#shipping_method > li").length > 0) {
		var shipping_method = [];

		jQuery("#grastinDelivery").show();

		jQuery("#shipping_method > li").each(function(i, elem) {
			shipping_method.push({
				id: jQuery(elem).find("input").attr("id"),
				label: jQuery(elem).find("label").text(),
				value: jQuery(elem).find("input").attr("value"),
				checked: ((jQuery(elem).find("input").attr("checked") == "checked") ? true : false)
			});
		});

		jQuery.ajax({
	        type: "POST",
	        url: window.wp_data.ajax_url,
	        data: {
	            action : "gdfw_action",
	            methods: shipping_method
	        },
	        success: function (response) {
	        	if(response == "") {
	        		jQuery("#grastinDelivery").hide();
	        	} else {
					var deactive = ((jQuery("#grastinDelivery .gdfw-tab > button.active").data("type") == "pvz") ? "kur" : "pvz");

					jQuery("#grastinDelivery ul.gdfw-delivery").empty();

					jQuery.each(JSON.parse(response), function() {
						if(this.checked == "false") {
							var checked = "";
						} else {
							var checked = "active";
						}

						var html = "<li class=\"gdfw-li-"+ this.type +" "+ checked +"\" data-id=\""+ this.id +"\" data-type=\""+ this.type +"\" data-list=\""+ this.list +"\" data-param=\""+ this.name +"|"+ this.kind +"|"+ this.value +"|"+ this.cost +"|"+ this.days +"\">";
						html += "<span class=\"gdfw-delivery-list-check\"></span> ";
						html += "<div class=\"gdfw-delivery-list-logo\">";
						html += "<img src=\""+ this.image +"\" alt=\""+ this.name +"\" title=\""+ this.name +"\">";
						html += "</div>";
						html += "<div class=\"gdfw-delivery-list-cost\">";

						var daysN = (this.days * 1);
						if(daysN != 0) {
							if(daysN == 1) {
								var dayName = "день";
							} else if(daysN > 1 && daysN < 5) {
								var dayName = "дня";
							} else {
								var dayName = "дней";
							}
							html += "за <span>"+ daysN +"</span> "+ dayName +" ";
						}
						html += "<span>" + this.cost +"</span> руб</div>";
						html += "</li>";

						jQuery("#grastinDelivery ul.gdfw-delivery").append(html);
					});

					jQuery("#grastinDelivery ul.gdfw-delivery > li.gdfw-li-"+ deactive).hide();

			        ymaps.ready(function(){
			        	if(myMap) {
			        		myMap.destroy();
			        	}

				        myMap = new ymaps.Map("gdfw-map", {
				            center: [55.76, 37.64],
				            zoom: 8,
				            controls: ["zoomControl"]
				        });

						var initCity = ymaps.geocode(billing_city).then(
							function (e) {
								var i = e.geoObjects.get(0);
								myMapCenter = i.geometry.getCoordinates();
								myMap.setCenter(myMapCenter, 8);

								myMap.geoObjects.removeAll();
							}
						);
					});

					jQuery("#grastinDelivery ul.gdfw-delivery > li").click(function() {
						gdfw_clusteronmap( this );
					});

					setTimeout(function() {
						jQuery("ul.gdfw-delivery > li:visible").eq(0).trigger("click");
					}, 1500);
				}
	        }
	    });
	} else {
		jQuery("#grastinDelivery").hide();
	}

	gdfw_loader();
	}, 2000);
}

function gdfw_change(obj) {
	if(jQuery(obj).hasClass("active") == false) {
		var type = jQuery(obj).data("type");

		if(myMap) {
			myMap.setCenter(myMapCenter, 9);
		}
		jQuery("#gdfw_shipping_method").val("");

		jQuery(obj).addClass("active").siblings().removeClass("active");

		jQuery("ul.gdfw-delivery > li").hide();
		jQuery("ul.gdfw-delivery > li.gdfw-li-"+ type).show();

		jQuery("ul.gdfw-delivery > li:visible").eq(0).trigger("click");
	}
}

function gdfw_loader() {
	jQuery("#grastinDelivery .gdfw-block-loading").fadeToggle("fast");
}

function gdfw_showonmap( obj ) {
	var sid = jQuery(obj).data("id");
	if(myMap && myPlacemarks) {
		jQuery("#grastinDelivery .gdfw-list-btn").trigger("click");
		var search = myGeoQuery.search("properties.objId = \"" + sid + "\"").getCenter(myMap);
		myMap.setCenter(search, 18);
	}
}

function gdfw_clusteronmap(obj) {
	gdfw_loader();

	jQuery("#gdfw_shipping_method").val("");

	var id = jQuery(obj).data("id");
	var list = jQuery(obj).data("list");
	var billing_city = jQuery("#billing_city").val();

	if(myMap && billing_city != "") {
		jQuery("input#" + id).trigger("click");

		myMap.geoObjects.removeAll();

		console.log(list);

		if(list) {
			var data = jQuery(obj).data("param");

			jQuery.ajax({
		        type: "POST",
		        url: window.wp_data.ajax_url,
		        data: {
		            action : "gdfw_action_elements",
		            param: data,
		            type: jQuery(obj).data("type"),
		            city: billing_city
		        },
		        success: function (response) {
		        	if(response != "") {
		        		jQuery("#gdfw-list ul.gdfw-list-address").empty();

						var clusterer = new ymaps.Clusterer({
						    preset: "islands#violetClusterIcons",
						    groupByCoordinates: !1,
						    minClusterSize: 3
						}),
							counterItem = 0;

						myPlacemarks = [];
						myGeoQuery = null;

		        		jQuery.each(JSON.parse(response), function() {
					        myPlacemarks[counterItem] = new ymaps.Placemark([this.latitude, this.longitude], {
					                objId: this.id,
					                hintContent: this.name,
					                balloonContentHeader: this.title,
					                balloonContent: this.desc
					            }, {
					            preset: "islands#violetIcon",
					        });

							var html = "<li>";
							html += "<div class=\"gdfw-list-address-links\"><a data-id=\""+ this.id +"\" onclick=\"gdfw_showonmap(this);\">Показать на карте</a></div>";
							html += "<div class=\"gdfw-list-address-data\">";
            				html += "<span>"+ ((counterItem * 1) + 1) +".</span> <p>"+ this.name +"</p>";
            				if(this.schedule) {
            					html += "Время работы: "+ this.schedule;
            				}
            				html += "</div>";
							html += "</li>";

							jQuery("#gdfw-list ul.gdfw-list-address").append(html);

		        			counterItem++;
		        		});

		        		myMap.setCenter(myMapCenter, 8);
				        clusterer.add(myPlacemarks);
				        myMap.geoObjects.add(clusterer);
				        myGeoQuery = ymaps.geoQuery(myPlacemarks);
		        	}
		        }
		    });
		} else {
			gdfw_click(obj);
		}

		jQuery(obj).addClass("active").siblings().removeClass("active");
	}

	setTimeout(function(){
	  gdfw_loader();
	}, 2000);

}

function gdfw_click( obj ) {
	var input = jQuery("#gdfw_shipping_method");
	if(input.length > 0) {
		input.val("");

		var id = jQuery(obj).data("id");
		var type = jQuery(obj).data("type");
		var content = jQuery(obj).data("param");

		if(jQuery(obj).data("this")) {
			var address = jQuery(obj).data("this");
			jQuery(obj).css({ "background-color": "rgb(70, 19, 207)" }).html("Выбрано");
		} else {
			var address = '';
		}

		jQuery("#gdfw_shipping_tr").remove();

		setTimeout(function(){
			var contentEx = content.split('|');
			var html = '<tr id="gdfw_shipping_tr"><th>Доставка ' + contentEx[0];
			if(address != "") {
				html += ' - пункт: ' + address;
			}
			html += '</th><td>' + contentEx[3] + ' р.</td></tr>';

			jQuery(html).insertBefore(jQuery("table.shop_table tr.cart-subtotal"));

			input.val(id + "|" + type + "|" + content + "|" + address);
		}, 1000);
	}
}

var delay = (function(){
	var timer = 0;
	return function(callback, ms){
		clearTimeout (timer);
		timer = setTimeout(callback, ms);
	};
})();

jQuery(document).ready(function($) {
	gdfw_onloader();

	jQuery("#billing_city").keyup(function() {
		delay(function(){
			gdfw_onloader();
		}, 1000 );
	});

	jQuery("#grastinDelivery .gdfw-list-btn").click(function() {
		if(jQuery(this).hasClass("active")) {
			jQuery("#gdfw-list").hide();
			jQuery("#gdfw-map").show();
		} else {
			jQuery("#gdfw-map").hide();
			jQuery("#gdfw-list").show();
		}
		jQuery(this).toggleClass("active");
	});

	var now = new Date(),
		date = now.getDate(),
		hour = now.getHours(),
		day = now.getDay(),
		disabledDays = [0],
		disabledDate = false;

	if( hour > '15' ) {
		var disabledDate = new Date();
		if( day == '5' ) {
			disabledDate.setDate( new Date().getDate() + 2 );
		} else {
			disabledDate.setDate( new Date().getDate() + 1 );
		}
	}

	jQuery("#gdfw_shippingdate").datepicker({
		minDate: now,
		dateFormat: "dd.mm.yyyy",
		onRenderCell: function (date, cellType) {
			var isDisabled = false,
				currentDate = date.getDate();

		    if (cellType == 'day') {
		        var day = date.getDay(),
		            isDisabled = disabledDays.indexOf(day) != -1;
		    }

		    if( disabledDate && (disabledDate.getDate() == currentDate) ) {
		    	var isDisabled = true;
		    }

	        return {
	            disabled: isDisabled
	        }
		}
	});
});