��          \      �       �   y   �      C     T     g     y  8   �     �    �    �     �  .        ;  ,   W  T   �     �                                       Disable this method if the cost of the order is less than inputted value. Leave this field empty to allow any order cost. Grastin Delivery Min. cost of order Point of delivery Select the issue point The plugin for calculating delivery via Grastin service. Title Project-Id-Version: gdfw-post-calc
POT-Creation-Date: 2018-03-21 22:14+0600
PO-Revision-Date: 2018-03-21 22:14+0600
Last-Translator: 
Language-Team: NGMTi <proginc@inbox.ru>
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Метод доставки не будет показан, если стоимость заказа меньше введенного значения. Оставьте это поле пустым, чтобы разрешить любую стоимость заказа. Grastin доставка Минимальная сумма заказа Пункт доставки Выберите пункт доставки Плагин для расчета доставки через службу Grastin. Название метода 